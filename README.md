## NIVA Project description

NIVA project is dedicated to the developments of applications in support to CAP spanning different Use Cases and technologies (Earth Observation, API etc.)

## OpenEO project
This repository contains a test implementation of OpenEO API with few basic processes and the following custom functionalities implemented:
* Interfield: Analysis of anomalies at scale (list of polygons with crop class) on a single specific crop using reference data	
* Intrafield: Analysis of anomalies in the time-series of pixels of a parcel
* Time Analysis	Several tools to be applied to time series of data to extract features (zonal stats values, max, min, average ...) over input period
All the 3 are available as jobs and are triggered by a GeoJSON of polygons (where applicable) and providing an enriched GeoJSON with information extracted.
The OpenEO project is described in the official web [site](https://openeo.org/).

## Installation
1. Pre-requisite for the installation are a Linux VM (Virtual Machine) with at least ubuntu 18.04. In order to install the component also the miniconda3 environment should be installed as describe here [minconda3](https://docs.conda.io/en/latest/miniconda.html). After the installation, the next step is to create the conda environment:  
  `conda env create -f environment.yml`

2. Another pre-requisite is the [REDIS server](https://redis.io/). You need also to install it on the VM, this is used by RQ (Redis Queue) to run parallel asynchronous batch jobs and exploit the cores of the VM. To install REDIS just use the following command which should also activate it as a system service.
  `sudo apt-get install redis-server` 

3. (optional) Install Jupyter to run demo notebook:  
  `conda activate openeo_niva`  
  `conda install jupyter`

## Architecture Component
The architecture of the OpenEO Parcels API component is shown in the following figure   ![](images/openeo.png "OpenEO Parcels Internal Architecture"). 

## How to start the Component

1. Check `status` of redis-server and `start` if it's not active:  
  `systemctl status redis`

2. Change directory in project folder

3. Activate conda environment:  
   `conda activate openeo_niva`

4. Run RQ (Redis Queue) server:  
  `rq worker --with-scheduler`

5. Run Flask API:  
  `python -m swagger_server 8080 niva.yaml`

## Getting Started
Getting started with the component is easy. The documentation on the [API reference](https://openeo.org/documentation/1.0/developers/api/reference.html) is available on the official sites of [OpenEO](https://openeo.org/). Currenlty, the implementation is not a full development of the [OpenEO processes](https://openeo.org/documentation/1.0/processes.html) but a small implementation that makes available the following processes on Sentinel 2 L2A data (whole catalogue potentially):

* Aggregate spatial - [definition](https://openeo.org/documentation/1.0/processes.html#aggregate_spatial)
* Load collection - [definition](https://openeo.org/documentation/1.0/processes.html#load_collection)
* Mask - [definition](https://openeo.org/documentation/1.0/processes.html#mask)
* Ndvi - [definition](https://openeo.org/documentation/1.0/processes.html#ndvi)
* Save result - [definition](https://openeo.org/documentation/1.0/processes.html#save_result)  

and the custom processes:  

* Interfield - [definition](swagger_server/processes/definitions/interfield.json)
* Intrafield - [definition](swagger_server/processes/definitions/intrafield.json)  

More custom processes can be developed on the basis of feedbacks. Please open an issue to suggest improvements to the component.

## Dependencies
Dependencies are contained in the `environment.yml` file, major dependencies are: GDAL, rasterio, xarray, Flask, openeo, pystac, rq. 
The conda environment contains also the dependencies to run the Jupyter Notebook Exercises.

## Contribute
Contributions are welcome especially in the form of additional collections and additional processes that can be done in order to increase the capability of this component.
