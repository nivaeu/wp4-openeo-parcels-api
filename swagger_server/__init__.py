import logging
import os

from redis import Redis
from rq import Queue

from swagger_server import logs

logger = logging.getLogger()
logs = logs.Logs()
logs.setup_logging(logger)
logs.log_new_product_instance_id("")

SECRET_KEY = os.urandom(24)

# Define an executor to manage async requests
background_tasks = Queue(connection=Redis(), default_timeout=3600)
