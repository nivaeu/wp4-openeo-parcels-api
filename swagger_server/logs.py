import logging
import os
import sys
from typing import Optional

class ContextFilter(logging.Filter):
    """
    This is a filter which injects contextual information into the log.
    """
    def __init__(self, product_instance_id : str):
        self.product_instance_id = product_instance_id

    def filter(self, record):
        record.product_instance_id = self.product_instance_id
        return True

class Logs:
    """
    Class to manage product instance ID in logs of microservices
    """
    def __init__(self):
        self.sdouthdlr = None
        self.filter = None

    def setup_logging(self, logger):
        """
        Setup logging configuration of the input logger.
        Do it as soon as the microservices is up (__init__.py or __main__ method)
        """
        self.sdouthdlr = logging.StreamHandler(sys.stdout)
        
        formatter = logging.Formatter('%(asctime)s %(levelname)-8s [%(name)s] ProductInstanceId: %(product_instance_id)s | %(message)s')
        self.sdouthdlr.setFormatter(formatter)
        logger.addHandler(self.sdouthdlr)
        logger.setLevel(logging.INFO)

    def log_new_product_instance_id(self, product_instance_id : Optional[str] = None):
        """
        Log a new product instance ID.
        Use this function each time a microservices is managing a new workload
        """
        if self.filter: self.sdouthdlr.removeFilter(self.filter)
        self.filter = ContextFilter(product_instance_id)
        self.sdouthdlr.addFilter(self.filter)

