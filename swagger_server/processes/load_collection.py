import concurrent.futures
from concurrent.futures import ThreadPoolExecutor
import json
import logging
import os
import time
from uuid import uuid4

import geojson
import rasterio
import shapely.geometry
from pystac_client import Client
from rasterio import mask, warp
from shapely.ops import cascaded_union

from swagger_server.processes.utils.geometries import split_geojson
from swagger_server.processes.vrt import vrt_assembler

logger = logging.getLogger()
earthsearch84 = 'https://earth-search.aws.element84.com/v0'

os.environ['CURL_CA_BUNDLE'] = '/etc/ssl/certs/ca-certificates.crt'
os.environ['GDAL_VRT_ENABLE_PYTHON'] = 'YES'
os.environ['GDAL_DISABLE_READDIR_ON_OPEN'] = 'EMPTY_DIR'
os.environ['CPL_VSIL_CURL_ALLOWED_EXTENSIONS']= 'tif'
os.environ['AWS_DEFAULT_REGION'] = 'us-west-2'

# Multi-thread executor
N_THREADS = 8
executor = ThreadPoolExecutor(max_workers=N_THREADS, thread_name_prefix="VRT_executor")


def load_collection(arguments: dict):

    # Retrieve parameters from input dict
    collection_id = arguments["id"] 
    bands = arguments["bands"] 
    spatial_extent = arguments["spatial_extent"] 
    temporal_extent = arguments["temporal_extent"]

    # Import output datacube template
    with open('swagger_server/processes/datacube_template.json') as f:
        datacube = json.load(f)

    datacube["name"] = str(uuid4())
    datacube["dimensions"]["band"] = [{"name": band, "nodata" : 0} for band in bands]

    catalog = Client.open(earthsearch84)

    # Period
    period = "/".join([t if t is not None else ".." for t in temporal_extent])

    is_bbox = all(c in spatial_extent for c in ["west", "south", "east", "north"])

    if is_bbox:
        # Bounding Box
        bbox = [spatial_extent["west"], spatial_extent["south"], spatial_extent["east"], spatial_extent["north"]]
        bbox_polygon = shapely.geometry.box(*bbox)
        bbox_polygon = shapely.geometry.mapping(bbox_polygon)
        bbox_polygon = json.loads(json.dumps(bbox_polygon))
        
        features = [geojson.Feature(id = "0", geometry=bbox_polygon)]
        mysearch = catalog.search(collections=[collection_id], bbox=bbox, datetime=period)
    else:
        # GeoJSON
        features = split_geojson(spatial_extent)

        # Merge all the geometries of items for search purposes
        polygons = []
        for feature in features:
            polygons.append(shapely.geometry.asShape(feature["geometry"]))

        features_union = cascaded_union(polygons) 
        features_union = features_union.simplify(0.1)
        features_union = shapely.geometry.mapping(features_union)
        features_union = json.loads(json.dumps(features_union))
        
        mysearch = catalog.search(collections=[collection_id], intersects=features_union, datetime=period)

    datacube["dimensions"]["geometry"] = features

    logger.info(f"{mysearch.matched()} items found")

    items = sorted([item for item in mysearch.items()], key=lambda i: i.properties["sentinel:product_id"])
    product_ids = set()

    # Choose orbit and tile with greatest intersection with input area
    # in case two intersections have the same area only the first is considered
    chosen_orbit_tiles = {}

    for feature in features:
        feature_id = feature['id']
        polygon = shapely.geometry.asShape(feature["geometry"]).buffer(0)
        max_area = 0

        for item in items:
            product_id = item.properties["sentinel:product_id"]

            # Add "t" dimension
            if product_id not in product_ids:
                product_ids.add(product_id)
                datacube["dimensions"]["t"].append({"product_id" : product_id, "datetime" : item.properties["datetime"]})

            orbit_tile_id = "_".join(product_id.split("_")[4:6])

            footprint = item.geometry
            footprint = shapely.geometry.asShape(footprint)

            area = footprint.intersection(polygon).area
            if area > max_area:
                chosen_orbit_tiles[feature_id] = orbit_tile_id
                max_area = area

        logger.debug(f"Tile with greatest intersection with input area '{feature_id}' is '{chosen_orbit_tiles[feature_id]}'")


    # Download values of all items found
    threads = []
    for feature in features:
        t = executor.submit(load_datacube, feature, items, bands, chosen_orbit_tiles)
        threads.append(t)

    subdatacubes = {}
    for future in concurrent.futures.as_completed(threads):
        feature_id, subdatacube = future.result()
        subdatacubes[feature_id] = subdatacube

    # Join datacube in sub-datacube
    for feature in features:
        feature_id = feature['id']
        feature_array = []

        for item in items:
            product_id = item.properties["sentinel:product_id"]

            subdatacube = subdatacubes[feature_id][product_id]
            feature_array.append(subdatacube["array"])

            # Use only last profile because resolution is the same for all bands
            profile_key = f'{product_id}_{feature_id}'
            if "profile" in subdatacube:
                datacube["profiles"][profile_key] = subdatacube["profile"]

        datacube["data"].append(feature_array)
    return datacube


def load_datacube(feature, items, bands, chosen_orbit_tiles):
    """
    Load datacube for a single feature
    """
    start_time = time.time()

    feature_id = feature['id']
    subdatacube = {}

    for item in items:
        product_id = item.properties["sentinel:product_id"]
        orbit_tile_id = "_".join(product_id.split("_")[4:6])
        image_array = []
        
        subdatacube[product_id] = {}

        # Skip tiles that were not chosen
        if orbit_tile_id == chosen_orbit_tiles[feature_id]:            
            for band in bands:
                vrt = vrt_assembler.create_VRT(item, band, resolution=10)

                with rasterio.open(vrt) as vrt_file:
                    profile = vrt_file.profile.copy()
                    reprojected_feature = warp.transform_geom({'init': 'epsg:4326'}, vrt_file.crs, feature["geometry"])
                    index_array, transform = mask.mask(vrt_file, [reprojected_feature], nodata=0, crop=True)
                    
                    # Change driver, transform, dimensions, no data value
                    profile["driver"] = "GTiff"
                    profile["transform"] = transform
                    _, profile["height"], profile["width"] = index_array.shape
                    profile["nodata"] = 0

                    image_array.append(index_array.squeeze().tolist())
            
            subdatacube[product_id]["profile"] = profile

        subdatacube[product_id]["array"] = image_array

    elapsed_time = round(time.time() - start_time, 2)
    logger.info(f"Downloaded data for input area '{feature_id}' from image {product_id}! ({elapsed_time} seconds)")

    return feature_id, subdatacube
