"""
Module used to create VRT starting from STAC Items.
"""
import os
import pystac
import logging
import affine
from .vrt_components import templates, INDICES, REF_BANDS
from typing import List, Dict
from string import Template

logger = logging.getLogger(__name__)

os.environ["AWS_NO_SIGN_REQUEST"] = "true"

def extract_SRS(item_properties: Dict):
    """
    A function to extract CRS.

    :param item_properties: properties of the
    stac item of interest.
    """
    epsg = item_properties["proj:epsg"]
    return f"EPSG:{epsg}"


def fill_simple_source(band: pystac.Asset, band_name: str, ref_band: pystac.Asset):
    """
    A function to fill simple source template
    with the correct parameters.

    :param band: asset of band of interest
    :param band_name: name of band
    :param ref_band: asset of reference band
    """
    band_res = band.properties["proj:transform"][0]
    ref_band_res = ref_band.properties["proj:transform"][0]

    if band_res >= ref_band_res:
        resampling = "average"
    else:
        resampling = "nearest"

    band_x_src, band_y_src = band.properties["proj:shape"]
    band_x_dst, band_y_dst = ref_band.properties["proj:shape"]
    block_x = block_y = REF_BANDS[ref_band_res][1]["block"]
    band_href = band.href.replace(
        "https://sentinel-cogs.s3.us-west-2.amazonaws.com", "/vsis3/sentinel-cogs"
    )
    band_dtype = "Byte" if band_name == "SCL" else "UInt16"
    return templates["source_template"].substitute(
        resampling=resampling,
        band_x_src=band_x_src,
        band_y_src=band_y_src,
        band_x_dst=band_x_dst,
        band_y_dst=band_y_dst,
        block_x=block_x,
        block_y=block_y,
        band=band_href,
        band_dtype=band_dtype,
    )


def create_VRT(
    item: pystac.Item, datum: str, resolution: int = None
):
    """
    A function to write a VRT of the datum
    of interest.

    :param item: STAC Item
    :param datum: datum of interest
    :param out_directory: output directory
    :param resolution: output resolution

    :rtype: str (output filename)
    """
    assets = item.get_assets()

    # Assign default output resolution if not explicit
    if resolution is None:
        if datum in INDICES:
            resolution = max(
                [
                    assets.get(i).properties["proj:transform"][0]
                    for i in INDICES[datum][1]["bands"]
                ]
            )
        elif datum in assets:
            resolution = assets.get(datum).properties["proj:transform"][0]
        else:
            raise KeyError(f"Datum {datum} not available")

    ref_band = REF_BANDS[float(resolution)][0]

    transform = assets[ref_band].properties["proj:transform"]
    a_coef, b_coef, c_coef, d_coef, e_coef, f_coef = affine.Affine(
        *transform[:6]
    ).to_gdal()
    x_dest, y_dest = assets[ref_band].properties["proj:shape"]

    vrt_subclass = ' subClass="VRTDerivedRasterBand"' if datum in INDICES else ""
    out_datatype = "UInt16"
    if datum in INDICES:
        out_datatype = "Int16"
    elif datum == "SCL":
        out_datatype = "Byte"

    vrt_filled = templates["base_template"].substitute(
                b_x_dst=x_dest,
                b_y_dst=y_dest,
                srs=extract_SRS(item.properties),
                a_coef=a_coef,
                b_coef=b_coef,
                c_coef=c_coef,
                d_coef=d_coef,
                e_coef=e_coef,
                f_coef=f_coef,
                dtype=out_datatype,
                subclass=vrt_subclass,
            )


    if datum in INDICES:
        vrt_filled = Template(
        '''$vrt_1
        $vrt_2''').substitute(vrt_1=vrt_filled, vrt_2=templates[INDICES[datum][0]].substitute(scale=10000, dtype="int16"))

        for b in INDICES[datum][1]["bands"]:
            vrt_filled = Template(
            '''$vrt_1
            $vrt_2''').substitute(vrt_1=vrt_filled, vrt_2=fill_simple_source(assets[b], b, assets[ref_band]))
            
    else:
        vrt_filled = Template(
        '''$vrt_1
        $vrt_2''').substitute(vrt_1=vrt_filled, vrt_2=fill_simple_source(assets[datum], datum, assets[ref_band]))

    vrt_filled = Template(
    '''$vrt_1
    $vrt_2''').substitute(vrt_1=vrt_filled, vrt_2=templates["close_template"].substitute())

    return vrt_filled
