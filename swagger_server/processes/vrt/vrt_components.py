from string import Template

INDICES = dict(
    ndvi=["normalized_difference", dict(bands=["B08", "B04"])],
    ndmi=["normalized_difference", dict(bands=["B08", "B11"])],
    ndsi=["normalized_difference", dict(bands=["B03", "B11"])],
    ndre=["normalized_difference", dict(bands=["B08", "B8A"])],
    cire=["ratio", dict(bands=["B08", "B8A"])],
    cig=["ratio", dict(bands=["B08", "B03"])],
    swi=["ratio", dict(bands=["B02", "B11"])],
    sdi=["ratio", dict(bands=["B08", "B11"])],
    msavi=["msavi", dict(bands=["B08", "B04"])],
)

REF_BANDS = {
    10.0: ["B04", dict(block=1024)],
    20.0: ["B11", dict(block=512)],
    60.0: ["B09", dict(block=256)],
}

templates = dict(
    base_template=Template(
        """
    <VRTDataset rasterXSize="$b_x_dst" rasterYSize="$b_y_dst">
    <SRS dataAxisToSRSAxisMapping="1,2">$srs
    </SRS>
    <GeoTransform> $a_coef, $b_coef, $c_coef, $d_coef, $e_coef, $f_coef </GeoTransform>
    <VRTRasterBand dataType="$dtype" band="1"$subclass>
    """
    ),
    close_template=Template(
        """
    </VRTRasterBand>
    </VRTDataset>
    """
    ),
    source_template=Template(
        """
        <SimpleSource resampling="$resampling">
        <SourceFilename relativeToVRT="0">$band</SourceFilename>
        <SourceBand>1</SourceBand>
        <SourceProperties RasterXSize="$band_x_src" RasterYSize="$band_y_src" DataType="$band_dtype" BlockXSize="$block_x" BlockYSize="$block_y" />
        <SrcRect xOff="0" yOff="0" xSize="$band_x_src" ySize="$band_y_src" />
        <DstRect xOff="0" yOff="0" xSize="$band_x_dst" ySize="$band_y_dst" />
        </SimpleSource>
    """
    ),
    normalized_difference=Template(
        """
    <PixelFunctionLanguage>Python</PixelFunctionLanguage>
    <PixelFunctionType>normalized_difference</PixelFunctionType>
    <PixelFunctionArguments scale="$scale" dtype="$dtype"/>
    <PixelFunctionCode>
      <![CDATA[
import numpy as np
def normalized_difference(in_ar, out_ar, xoff, yoff, xsize, ysize, raster_xsize, raster_ysize, buf_radius, gt, **kwargs):
  scale = int(kwargs["scale"])
  dtype = kwargs["dtype"]
  b1, b2 = in_ar
  np.clip(np.nan_to_num(np.round((b1 - b2).astype(float) / (b1 + b2) * scale), nan=np.iinfo(dtype).min).astype(dtype), -scale, scale, out_ar)
        ]]>
    </PixelFunctionCode>
    <ColorInterp>Gray</ColorInterp>
    """
    ),
    ratio=Template(
        """
    <PixelFunctionLanguage>Python</PixelFunctionLanguage>
    <PixelFunctionType>ratio</PixelFunctionType>
    <PixelFunctionArguments scale="$scale" dtype="$dtype"/>
    <PixelFunctionCode>     
      <![CDATA[
import numpy as np
def ratio(in_ar, out_ar, xoff, yoff, xsize, ysize, raster_xsize, raster_ysize, buf_radius, gt, **kwargs):
  scale = int(kwargs["scale"])
  dtype = kwargs["dtype"]
  b1, b2 = in_ar
  np.clip(np.nan_to_num(np.round(b1.astype(float) / b2 * scale), nan=np.iinfo(dtype).min).astype(dtype), 0, None, out_ar)
        ]]>
    </PixelFunctionCode>
    <ColorInterp>Gray</ColorInterp>
    """
    ),
    msavi=Template(
        """
    <PixelFunctionLanguage>Python</PixelFunctionLanguage>
    <PixelFunctionType>msavi</PixelFunctionType>
    <PixelFunctionArguments scale="$scale" dtype="$dtype"/>
    <PixelFunctionCode>     
      <![CDATA[
import numpy as np
def msavi(in_ar, out_ar, xoff, yoff, xsize, ysize, raster_xsize, raster_ysize, buf_radius, gt, **kwargs):
  scale = int(kwargs["scale"])
  dtype = kwargs["dtype"]
  b1, b2 = in_ar
  np.clip(np.nan_to_num(np.round(((2*(b1.astype(float)/10000) + 1 - np.sqrt((2*(b1.astype(float)/10000)+1)**2 - 8*((b1.astype(float)/10000) - b2.astype(float)/10000))) / 2)*scale), nan=np.iinfo(dtype).min).astype(dtype), -scale, scale, out_ar)
        ]]>   
    </PixelFunctionCode>
    <ColorInterp>Gray</ColorInterp>
    """
    ),
)
