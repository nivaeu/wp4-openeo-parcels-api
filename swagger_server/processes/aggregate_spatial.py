import logging
from copy import deepcopy

import geojson
import numpy as np
from scipy import stats

logger = logging.getLogger()


available_stats = {
    'count_value': lambda x: x.size - np.count_nonzero(np.isnan(x)),
    'mean_value': lambda x: round(np.nanmean(x), 4),
    'variance_value': lambda x: round(np.nanvar(x, ddof=1), 4),
    'min_value': lambda x: round(np.nanmin(x), 4),
    'first_quartile_value': lambda x: round(np.nanpercentile(x,25), 4),
    'median_value': lambda x: round(np.nanpercentile(x,50), 4),
    'third_quartile_value': lambda x: round(np.nanpercentile(x,75), 4),
    'max_value': lambda x: round(np.nanmax(x), 4),
    'skewness_value': lambda x: round(float(stats.skew(x, axis = None, nan_policy='omit')), 4),
    'kurtosis_value': lambda x: round(float(stats.kurtosis(x, axis = None, nan_policy='omit')), 4)
}


def aggregate_spatial(arguments:dict):
    """
        Zonal statistics for geometries
    """
    datacube = deepcopy(arguments["data"])

    items = datacube["dimensions"]["t"]
    bands = datacube["dimensions"]["band"]
    geometries = datacube["dimensions"]["geometry"]

    results = []

    for feature_index, feature in enumerate(geometries):
        geometry = datacube["data"][feature_index]

        for item_index, item in enumerate(items):

            t = geometry[item_index]

            if not t: continue

            for band_index, band_info in enumerate(bands):
                band = np.array(t[band_index]).astype(float)
                band[band == band_info["nodata"]] = np.nan

                band_stats = {}
                for stat in available_stats:
                    band_stats[stat] = available_stats[stat](band)

            if band_stats["count_value"] > 0:
                feature_id = f"{item['product_id']}_{feature['id']}"

                properties = {}
                properties.update(band_stats)
                properties["datetime"] = item["datetime"]
                properties["product_id"] = item["product_id"]
                properties["feature_id"] = feature['id']

                output_feature = geojson.Feature(id = feature_id, geometry = feature["geometry"], properties = properties)
                results.append(output_feature)
            
    results = geojson.FeatureCollection(results)
    return results
