import json
import logging
import os
from copy import deepcopy

import geojson
import geopandas as gpd
import numpy as np

logger = logging.getLogger()


def intrafield(arguments:dict):
    """
        Crop failure analysis based on NDVI
    """
    datacube = deepcopy(arguments["data"])
    stats = deepcopy(arguments["stats"])

    items = datacube["dimensions"]["t"]
    bands = datacube["dimensions"]["band"]
    geometries = datacube["dimensions"]["geometry"]

    # Red band index
    try:
        ndvi_index = [band["name"] for band in bands].index("ndvi")
        ndvi_nodata = bands[ndvi_index]["nodata"]
    except ValueError:
        raise ValueError("Input datacube has no NDVI band")

    results = []

    # Read stats as GeoDataFrame
    stats_ds = gpd.GeoDataFrame.from_features(stats["features"])

    for feature_id, group in stats_ds.groupby(by="feature_id"):
        feature_id_index = [geom["id"] for geom in geometries].index(feature_id)
        properties = {}

        try:
            max_df = find_maximum(group)

            datetime_index = [item["datetime"] for item in items].index(max_df["datetime"].iloc[0])
            max_ndvi_datacube = np.array(datacube["data"][feature_id_index][datetime_index][ndvi_index])

            crop_failure = calculate_crop_failure(max_ndvi_datacube, max_df["mean_value"].iloc[0], nodata = ndvi_nodata)
            crop_failure_stats = get_crop_failure_stats(crop_failure, max_df["count_value"].iloc[0])
            
            properties["datetime"] = max_df["datetime"].iloc[0]
            properties["count_value"] = int(max_df["count_value"].iloc[0])
            properties["mean_value"] = round(float(max_df["mean_value"].iloc[0]), 4)
            # used to generate intrafield images
            properties["intrafield_array"] = crop_failure.tolist()
        
        except ValueError:
            crop_failure_stats = {'green': None, 'orange': None, 'red': None}
            dates = [item["datetime"] for item in items]
            properties["start_datetime"], properties["end_datetime"] = min(dates), max(dates)
            properties["count_value"] = None
            properties["mean_value"] = None

        properties["intrafield"] = crop_failure_stats

        output_feature = geojson.Feature(id = feature_id, geometry = geometries[feature_id_index]["geometry"], properties = properties)
        results.append(output_feature)

    results = geojson.FeatureCollection(results)
    return results


def find_maximum(stats_df):
    """
    This function returns a dataframe (1 row)
    with info about the acquisition with the maximum
    index value. Saturated acquisition are removed 
    and only ndvi values bigger than 0.4 are considered
    eligible for the analysis (no bare soil).
    :param stats_df: stats dataframe
    :rtype: dataframe
    """
    max_df = stats_df.query('mean_value != 1 & mean_value >= 0.4') # remove saturated values, no bare soil
    if max_df.empty:
        raise ValueError("No data eligible for Crop Failure")
    max_df = max_df[(max_df['count_value'] > (max_df['count_value'].max())*0.7)] 
    max_df = max_df[max_df['mean_value'] == max_df['mean_value'].max()]
    if max_df.empty:
        raise ValueError("No data eligible for Crop Failure")
    return max_df


def calculate_crop_failure(polygon_array, mean_value, nodata = np.nan):
    """
    This function returns an array with pixels
    classified as green (1), orange (2), red (3).
    Nodata --> 255
    :param polygon_array: ndvi array
    :param mean_value: mean value
    :rtype: numpy array
    """
    orange = round((float(mean_value)/100)*20, 2)
    red = round((float(mean_value)/100)*30, 2)
    polygon_array[polygon_array == nodata] = 255    
    mask2 = ((mean_value - orange < polygon_array) & (polygon_array < 255))
    polygon_array[mask2] = 1 
    mask1 = ((mean_value - red < polygon_array) & (polygon_array <= mean_value - orange))
    polygon_array[mask1] = 2
    polygon_array[polygon_array <= mean_value - red] = 3
    return polygon_array


def get_crop_failure_stats(crop_failure_array, count_value):
    """
    This function returns a dictionary with
    percentages of pixels that are below 20% of mean value
    and 30% of mean value.
    :param crop_failure_array: crop failure array
    :param count_value: count value
    :rtype: dict
    """
    return {'green': round(float(np.count_nonzero(crop_failure_array == 1)/count_value)*100 , 4),
            'orange': round(float(np.count_nonzero(crop_failure_array == 2)/count_value)*100 , 4),
            'red': round(float(np.count_nonzero(crop_failure_array == 3)/count_value)*100 , 4)}
