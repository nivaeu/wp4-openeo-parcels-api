import logging
from copy import deepcopy

import geojson
import pandas as pd
import geopandas as gpd
import numpy as np

from dtaidistance import dtw
from dtaidistance import ed

logger = logging.getLogger()


def interfield(arguments:dict):
    """
        Anomaly detection between NDVI timeseries
    """
    stats = deepcopy(arguments["stats"])

    geometries = {}
    for feature in stats["features"]:
        feature_id = feature["properties"]["feature_id"]
        if feature_id not in geometries:
            geometries[feature_id] = feature["geometry"]

    # Read stats as GeoDataFrame
    stats_ds = gpd.GeoDataFrame.from_features(stats["features"])

    # Generate NDVI timeseries upsampled to day frequency
    features_timeseries = None

    for feature_id, group in stats_ds.groupby(by="feature_id"):
        timeseries = upsample_interpolate_df(group)
        timeseries["feature_id"] = feature_id

        if features_timeseries is None:
            features_timeseries = timeseries
        else:
            features_timeseries = features_timeseries.append(timeseries)

    # Generate reference NDVI timeseries
    reference_timeseries = None

    for dt, group in features_timeseries.groupby(by="datetime"):
        median_df = pd.DataFrame({
            "median_value" : group.agg({'median_value': 'mean'}).iloc[0]
        }, index=[dt])

        if reference_timeseries is None:
            reference_timeseries = median_df
        else:
            reference_timeseries = reference_timeseries.append(median_df)
    
    # Anomaly Detection
    results = []

    for feature_id, group in features_timeseries.groupby(by="feature_id"):
        timeseries = group.set_index('datetime')

        anomaly = None
        try:
            timeseries, reference_intersection = get_same_starting_ending_point(timeseries, reference_timeseries)
        except ValueError:
            anomaly = {'distance_difference': None, 'distance_from_reference': None, 'max_distance': None}

        if anomaly is None: anomaly = calculate_DTW(timeseries, reference_intersection)

        properties = {}
        properties["interfield"] = anomaly
        properties["datetime"] = None
        properties["start_datetime"] = timeseries.index.min().isoformat() + 'Z'
        properties["end_datetime"] = timeseries.index.max().isoformat() + 'Z'

        output_feature = geojson.Feature(id = feature_id, geometry = geometries[feature_id], properties = properties)
        results.append(output_feature)

    results = geojson.FeatureCollection(results)
    return results



def upsample_interpolate_df(df):
    """
    This function returns a dataframe pandas
    daily resampled.
    :param df: Dataframe created from stats["values"]
    :rtype: dataframe with daily upsampling and interpolation
    """
    df = pd.DataFrame(df)
    df["datetime"] = pd.to_datetime(df["datetime"], format='%Y-%m-%dT%H:%M:%SZ')
    df = df.sort_values("datetime")
    df.index=df["datetime"]

    # Upsampling - daily
    df_daily = df.resample('D').mean()

    # Linear Interpolation
    df_daily_interpolated = df_daily.interpolate(method='linear')
    df_daily_interpolated.reset_index(inplace=True)
    return df_daily_interpolated


def get_same_starting_ending_point(feature_df, reference_df):
    """
    This function returns two dataframes aligned in time.
    :param feature_df: Dataframe of data related to feature 
                        object of analysis
    :param reference_df: Dataframe of reference data
    :rtype: two dataframe (feature and reference) defined only
            over dates intersection
    """
    intersection_index = np.intersect1d(feature_df.index, reference_df.index)
    if len(intersection_index) == 0:
        raise ValueError('Empty dates intersection')
    feature_df = feature_df[feature_df.index.isin(intersection_index)]
    reference_df = reference_df[reference_df.index.isin(intersection_index)]
    return feature_df, reference_df


def calculate_DTW(geometry_df, reference_df, stats='median_value', window=20):
    """
    This function calculates the distance between two timeseries.
    :param geometry_df: geometry dataframe (output of the function above)
    :param reference_df: reference dataframe (output of the function above)
    :param stats: stats to be considedered for the analysis
    :param window: window in days for DTW
    :rtype: dict with result of interfield analysis
    """
    d, _ = dtw.warping_paths(reference_df[stats].values, geometry_df[stats].values, window=window)
    # Find the upper bound distance given the reference trend
    worst_trend = np.array([0 if x > 0.5 else 1 for x in reference_df[stats].values])
    upper_bound = ed.distance(reference_df[stats].values, worst_trend)
    if d >= upper_bound*0.5:
        return {'distance_difference': 'high', 'distance_from_reference': round(d,3), 'max_distance': round(upper_bound, 3)}
    elif upper_bound*0.25 <= d < upper_bound*0.5:
        return {'distance_difference': 'medium', 'distance_from_reference': round(d,3), 'max_distance': round(upper_bound, 3)}
    else: 
        return {'distance_difference': 'low', 'distance_from_reference': round(d,3), 'max_distance': round(upper_bound, 3)}