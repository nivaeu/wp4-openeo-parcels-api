import logging
from copy import deepcopy
from uuid import uuid4

import numpy as np

logger = logging.getLogger()


def mask(arguments:dict):

    datacube = deepcopy(arguments["data"])
    datacube_mask = deepcopy(arguments["mask"])

    results = []
    items = datacube["dimensions"]["t"]
    bands = datacube["dimensions"]["band"]
    geometries = datacube["dimensions"]["geometry"]

    results = []

    for feature_index, feature in enumerate(geometries):
        geometry = datacube["data"][feature_index]
        geometry_mask = datacube_mask["data"][feature_index]
        geometry_array = []

        for item_index, item in enumerate(items):
            t = geometry[item_index]
            t_mask = geometry_mask[item_index]
            date_array = []

            if t: 
                scl = np.array(t_mask[0])

                for band_index, band_info in enumerate(bands):
                    band = np.array(t[band_index]).astype(float)
                    nodata = band_info["nodata"]

                    cloudmask = np.isin(scl.astype(int), [0,1,3,8,9,10,11])
                    masked_band = np.ma.array(band, mask=cloudmask).filled(nodata)
                        
                    date_array.append(masked_band.tolist()) 

            # append a list with an element to keep band dimension
            geometry_array.append(date_array)

        results.append(geometry_array)

    datacube["data"] = results

    datacube["name"] = str(uuid4())

    return datacube
