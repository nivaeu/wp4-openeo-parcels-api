import geojson

def _check_geometry(feature_id, geometry_type):
    if geometry_type != 'Polygon':
        raise ValueError(f'Input GeoJSON must be a collection of "Polygon"s ("{geometry_type}" found)')

    if feature_id is None:
        raise ValueError(f'You have to specify the "id" field in input geometries')

    return True

def split_geojson(spatial_extent : dict):
    """
    Parse input GeoJSON dictionary and split collections in single features

    :param spatial_extent: A GeoJSON dictionary
    :return: a list of Features
    """

    if spatial_extent["type"] == "Polygon":
        feature_id = spatial_extent.pop("id", None)
        _check_geometry(feature_id, spatial_extent["type"])

        features = [geojson.Feature(id = feature_id, geometry = spatial_extent)]

    elif spatial_extent["type"] == "Feature":
        feature_id = spatial_extent.pop("id", None)
        _check_geometry(feature_id, spatial_extent["geometry"]["type"])

        features = [geojson.Feature(id = feature_id, geometry = spatial_extent["geometry"])]

    # Split multi-geometries
    elif spatial_extent["type"] == "GeometryCollection":
        features = []
        for geometry in spatial_extent["geometries"]:
            feature_id = geometry.pop("id", None)
            _check_geometry(feature_id, geometry["type"])
            feature = geojson.Feature(id = feature_id, geometry=geometry)

            features.append(feature)

    elif spatial_extent["type"] == "FeatureCollection":
        features = []
        for feature in spatial_extent["features"]:
            feature_id = feature.pop("id", None)
            _check_geometry(feature_id, feature["geometry"]["type"])
            feature = geojson.Feature(id = feature_id, geometry=feature["geometry"])
            
            features.append(feature)

    else:
        raise ValueError(f"Input GeoJSON of type '{spatial_extent['type']}' is not supported")

    return features