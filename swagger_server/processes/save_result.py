import json
import logging
import os
from copy import deepcopy
from urllib.parse import urljoin, urlparse
from uuid import uuid4

import numpy as np
import rasterio
from rasterio.profiles import DefaultGTiffProfile

logger = logging.getLogger()


def save_result(arguments: dict, output_directory : str, is_result : bool):
    """
    Save results
    """
    data = deepcopy(arguments["data"])

    out_format = "JSON"
    if "format" in arguments and arguments["format"] is not None:
        out_format = deepcopy(arguments["format"])

    available_output_formats = ["JSON", "GTiff"]
    if out_format not in available_output_formats:
        raise ValueError(f'Parameter "format" must be valorized as one of {available_output_formats}. (input value: "{out_format}")')

    available_input_data_types = ["raster-datacube", "FeatureCollection"]
    if "type" not in data or data["type"] not in available_input_data_types:
        raise ValueError(f'Results must have "type" field valorized as one of {available_input_data_types}')

    results = []

    # Save GeoTIFF images if requested 
    if out_format == "GTiff":
        if data["type"] == "raster-datacube":
            results.extend(save_rasterdatacube_as_geotiff(data, output_directory))
            # remove data from output JSON, saving only metadata
            data.pop("data")
            data["profiles"] = {profile : serialize_profile(data["profiles"][profile]) for profile in data["profiles"]}
        else:
            logger.warning(f'Format "{out_format}" is available only for "raster-datacube" data type. Switched to "JSON" format')

    # Save always a datacube/metadata JSON
    dst_filepath = os.path.join(output_directory, 'output.json')
    with open(dst_filepath, "w+") as f:
        f.write(json.dumps(data, indent=2))
    results.append(dst_filepath)
    

    if is_result:
        return results
    else:
        return arguments["data"]


def save_rasterdatacube_as_geotiff(data : dict, output_directory : str):
    """
    Save a raster datacube as several GeoTIFF images
    """
    results = []

    items = data["dimensions"]["t"]
    bands = data["dimensions"]["band"]
    geometries = data["dimensions"]["geometry"]

    for feature_index, feature in enumerate(geometries):
        geometry = data["data"][feature_index]

        for item_index, item in enumerate(items):
            t = geometry[item_index]

            if not t: continue

            profile_key = f'{item["product_id"]}_{feature["id"]}'
            profile = deepcopy(data["profiles"][profile_key])

            dst_filename = os.path.join(output_directory, f'{profile_key}.tif')

            # Update bands count and type
            profile["count"] = len(bands)

            if len(bands) == 1 and bands[0]["name"] == "ndvi":
                dtype = np.dtype(bands[0]["dtype"])
                profile["nodata"] = bands[0]["nodata"]
                profile["dtype"] = dtype.name
            else:
                dtype = np.dtype(profile["dtype"])

            # Write GeoTIFF image
            with rasterio.open(dst_filename, 'w', **profile) as dst_dataset:
                for band_index, band_info in enumerate(bands):
                    band = np.array(t[band_index]).astype(dtype)
                    dst_dataset.write_band(band_index + 1, band)

            results.append(dst_filename)

    return results


def serialize_profile(original_profile):
    profile = deepcopy(original_profile)

    profile = dict(**profile)
    profile.update(crs=profile["crs"].to_wkt(), transform=list(profile["transform"]))

    return profile