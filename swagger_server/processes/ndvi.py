import logging
from copy import deepcopy
from uuid import uuid4

import numpy as np

logger = logging.getLogger()

SCALE = 1
DTYPE = float

def ndvi(arguments:dict):

    datacube = deepcopy(arguments["data"])

    bands = datacube["dimensions"]["band"]
    # Red band index
    try:
        red = [band["name"] for band in bands].index("B04")
    except ValueError:
        raise ValueError("Input datacube has no red band")

    # NIR band index
    try:
        nir = [band["name"] for band in bands].index("B08")
    except ValueError:
        raise ValueError("Input datacube has no NIR band")

    if DTYPE not in [np.int16, np.int32, int, np.single, float]: raise ValueError(f"dtype '{DTYPE}' is not supported")
    if SCALE not in [1, 1000, 10000]: raise ValueError(f"scale '{SCALE}' is not supported")

    results = []
    for geometry in datacube["data"]:
        date_array = []

        for t in geometry:
            geometry_array = []

            if t:
                red_band = np.array(t[red])
                nir_band = np.array(t[nir])

                # Compute NDVI
                ndvi = (nir_band - red_band).astype(float) / (nir_band + red_band)

                if DTYPE in [np.int16, np.int32, int]:
                    ndvi = np.round(ndvi * SCALE)
                    nodata = np.iinfo(DTYPE).min

                elif DTYPE in [np.single, float]:
                    ndvi = ndvi * SCALE
                    nodata = np.finfo(DTYPE).min

                ndvi = np.clip(np.nan_to_num(ndvi, nodata).astype(DTYPE), -SCALE, SCALE)
                geometry_array.append(ndvi.tolist()) 

            # append a list with an element to keep band dimension
            date_array.append(geometry_array)

        results.append(date_array)

    datacube["data"] = results

    datacube["name"] = str(uuid4())
    datacube["dimensions"]["band"] = [{"name": "ndvi", "nodata" : nodata, "dtype" : np.dtype(DTYPE).name}]

    return datacube
