import json
import logging
import os
import uuid
from copy import deepcopy
from datetime import datetime

import connexion
import requests

from swagger_server import logs, background_tasks
from swagger_server.controllers.lib.auth import get_user_config_from_header
from swagger_server.controllers.lib.processing import self_request_validate_job, update_status, get_available_processes, execute_process_graph

OPENEO_PORT = os.environ.get("OPENEO_PORT")

# logging configuration
logger = logging.getLogger()

headers = {'Content-type': 'application/json'}

with open('swagger_server/json/jobs.json') as f:
    response_jobs = json.load(f)

with open('swagger_server/json/userdata.json') as f:
    user_data = json.load(f)

with open('swagger_server/json/config.json') as f:
    user_config = json.load(f)



def list_processes(limit=None):  # noqa: E501
    """List all CLEOS processing services

    The request asks the back-end for available predefined processes and returns detailed process descriptions, including parameters and return values. # noqa: E501

    :param limit: This parameter enables pagination for the endpoint and specifies the maximum number of elements that arrays in the top-level object (e.g. collections, processes, batch jobs, secondary services, log entries, etc.) are allowed to contain. The &#x60;links&#x60; array MUST NOT be paginated like the resources, but instead contain links related to the paginated resources or the pagination itself (e.g. a link to the next page). If the parameter is not provided or empty, all elements are returned.  Pagination is OPTIONAL: back-ends or clients may not support it. Therefore it MUST be implemented in a way that clients not supporting pagination get all resources regardless. Back-ends not supporting pagination MUST return all resources.  If the response is paginated, the &#x60;links&#x60; array MUST be used to communicate the links for browsing the pagination with pre-defined &#x60;rel&#x60; types. See the &#x60;links&#x60; array schema for supported &#x60;rel&#x60; types. Backend implementations can, unless specified otherwise, use all kind of pagination techniques, depending on what is supported best by their infrastructure: page-based, offset-based, token-based or something else. The clients SHOULD use whatever is specified in the links with the corresponding &#x60;rel&#x60; types.
    :type limit: int

    :rtype: Processes
    """

    _, user_config = get_user_config_from_header()

    available_processes = get_available_processes(user_config, as_dict=True)

    return available_processes, requests.codes.ok


def validate_job(body):
    """Validates a new satellite imagery/processing request

    :param body: Specifies the job details, e.g. the user-defined process and billing details.
    :type body: dict | bytes

    :rtype: None
    """

    _, user_config = get_user_config_from_header()
    available_processes = get_available_processes(user_config)

    service_ids = [body['process_graph'][p]["process_id"] for p in body['process_graph']]

    errors = []
    for service_id in service_ids:
        # From the /validation openeo endpoint:
        # "Please note that a validation always returns with HTTP status code 200.
        # Error codes in the 4xx and 5xx ranges MUST be returned only when the
        # general validation request is invalid (e.g. server is busy or properties
        # in the request body are missing), but never if an error was found during
        # validation of the user-defined process (e.g. an unsupported process)."

        if service_id not in available_processes:
            error_message = f"The service id '{service_id}' does not exist"
            logger.warning(error_message)
            errors.append({"code": "404", "message": error_message})    
    

    # All processes must start with load_collection
    if service_ids[0] != available_processes[0]:
        error_message = f"The requested pipeline is invalid. The first node must be \
            '{available_processes[0]}'"
        logger.warning(error_message)
        errors.append({"code": "400", "message": error_message})     
    
    # TODO: You can't have ndvi after aggregate_spatial

    return {"errors": errors}, requests.codes.ok


def create_job(body):  # noqa: E501
    """Create a new satellite imagery/processing request

    Creates a new batch processing task (job) from one or more (chained) processes at the back-end.  Processing the data doesn&#x27;t start yet. The job status gets initialized as &#x60;created&#x60; by default. # noqa: E501

    :param body: Specifies the job details, e.g. the user-defined process and billing details.
    :type body: dict | bytes

    :rtype: None
    """

    user_id, _ = get_user_config_from_header()
    auth_header = connexion.request.headers.get('Authorization')
    ERROR_CREATING_PROCESSING_REQUEST = "Error creating the processing request"

    try:
        product_instance_id = f"{str(uuid.uuid4())}"

        # Validate the request
        validation = self_request_validate_job(body["process"], auth_header)
        if validation: return validation

        # adding context to log
        logs.log_new_product_instance_id(product_instance_id)
        logger.info(f"New product instance [create] request received for user [{user_id}]")
        
        product_directory = f"swagger_server/jobs/{user_id}/{product_instance_id}"
        process_graph_filename = os.path.join(product_directory, "process_graph.json")
        status_filename = os.path.join(product_directory, "status.json")

        if not os.path.exists(process_graph_filename):
            os.makedirs(os.path.dirname(process_graph_filename), exist_ok=True)

            # save process graph
            with open(process_graph_filename, "w+") as f:
                f.write(json.dumps(body, indent=2))

            # set status to created
            with open(status_filename, "w+") as status:
                status.write(json.dumps({"status" : "created", "created": datetime.utcnow().isoformat(timespec="seconds") }))
        else:
            error_message = f"Duplicated product instance with id '{product_instance_id}'"
            logger.error(error_message)
            return {"message": ERROR_CREATING_PROCESSING_REQUEST, "description" : error_message}, requests.codes.bad

        header_response = {'Location': f"http://localhost:{OPENEO_PORT}/api/v1.0/jobs/{product_instance_id}",
                           'OpenEO-Identifier': product_instance_id}

        return None, requests.codes.created, header_response

    except KeyError:
        error_message = "Malformed input json"
        logger.exception(error_message)
        return {"message": ERROR_CREATING_PROCESSING_REQUEST, "description" : error_message}, requests.codes.bad

    except Exception as e:
        logger.exception(f"Unexpected error: {e}")
        return {"message" : ERROR_CREATING_PROCESSING_REQUEST}, requests.codes.internal_server_error

    finally:
        logs.log_new_product_instance_id()



def start_job(job_id):  # noqa: E501
    """Start a satellite imagery/processing request

    Adds a batch job to the processing queue to compute the results.  The result will be stored in the format specified in the process graph. To specify the format use a process such as &#x60;save_result&#x60;.  This endpoint has no effect if the job status is already &#x60;queued&#x60; or &#x60;running&#x60;. In particular, it doesn&#x27;t restart a running job. Processing MUST be canceled before to restart it.  The job status is set to &#x60;queued&#x60;, if processing doesn&#x27;t start instantly. * Once the processing starts the status is set to &#x60;running&#x60;.   * Once the data is available to download the status is set to &#x60;finished&#x60;.      * Whenever an error occurs during processing, the status MUST be set to &#x60;error&#x60;. # noqa: E501

    :param job_id: Unique job identifier.
    :type job_id: dict | bytes

    :rtype: None
    """
    user_id, _ = get_user_config_from_header()
    product_instance_id = job_id

    ERROR_STARTING_PROCESSING_REQUEST = "error starting the processing request"

    # adding context to log
    logs.log_new_product_instance_id(product_instance_id)

    logger.info(f"New product instance [start] request received for user [{user_id}]")

    try:
        product_directory = f"swagger_server/jobs/{user_id}/{product_instance_id}"
    
        process_graph_filename = os.path.join(product_directory, "process_graph.json")
        status_filename = os.path.join(product_directory, "status.json")

        if os.path.exists(process_graph_filename):
            with open(process_graph_filename, "r") as f:
                process_graph = json.load(f)
        else:
            error_message = "Failed to start the processing request. Product instance does not exist"
            logger.error(error_message)
            return {"message": ERROR_STARTING_PROCESSING_REQUEST, "description" : error_message}, requests.codes.not_found

        # set status to queued
        update_status(status_filename, "queued")

        background_tasks.enqueue(execute_process_graph, process_graph["process"],  user_id, product_instance_id)

        return None, requests.codes.accepted

    except requests.exceptions.RequestException as e:
        logger.exception(f"Unexpected error: {e}")
        return {"message" : ERROR_STARTING_PROCESSING_REQUEST, "description" : e.response.text}, e.response.status_code

    except Exception as e:
        logger.exception(f"Unexpected error: {e}")
        return {"message" : ERROR_STARTING_PROCESSING_REQUEST}, requests.codes.internal_server_error

    finally:
        #removing context from log
        logs.log_new_product_instance_id()


def compute_result(body):
    """Search and get pre-signed URL of items of interest

    A user-defined process will be executed directly and the result will be downloaded in the format specified in the process graph. This endpoint can be used to generate small previews or test user-defined processes before starting a batch job.
    Timeouts on either client- or server-side are to be expected for complex computations. Back-ends MAY send the openEO error `ProcessGraphComplexity` immediately if the computation is expected to time out. Otherwise requests MAY time-out after a certain amount of time by sending openEO error `RequestTimeout`.
    A header named `OpenEO-Costs` MAY be sent with all responses, which MUST include the costs for processing and downloading the data. Additionally,  a link to a log file MAY be sent in the header.

    :param body: Specifies the job details, e.g. the user-defined process and billing details.
    :type body: dict | bytes

    :rtype: None
    """
    user_id, _ = get_user_config_from_header()
    auth_header = connexion.request.headers.get('Authorization')

    # Validate the request
    validation = self_request_validate_job(body["process"], auth_header)
    if validation: return validation

    product_instance_id = f"{str(uuid.uuid4())}"

    return execute_process_graph(body["process"], user_id, product_instance_id)