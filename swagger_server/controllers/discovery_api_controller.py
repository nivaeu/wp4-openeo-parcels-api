import copy
import json
import logging
import os

import connexion
import requests
from swagger_server.http_requests import get_request

from swagger_server import util
from swagger_server.controllers.lib.auth import get_user_config_from_header

earthsearch84 = 'https://earth-search.aws.element84.com/v0'

# logging configuration
logger = logging.getLogger()


def list_collections(limit=None):
    """List all datasets available in CLEOS

    Lists available collections with at least the required information.  It is **strongly RECOMMENDED** to keep the response size small by omitting larger optional values from the objects in &#x60;collections&#x60; (e.g. the &#x60;summaries&#x60; and &#x60;cube:dimensions&#x60; properties). To get the full metadata for a collection clients MUST request &#x60;GET /collections/{collection_id}&#x60;.  This endpoint is compatible with [STAC 0.9.0](https://stacspec.org) and [OGC API - Features](http://docs.opengeospatial.org/is/17-069r3/17-069r3.html). [STAC API](https://github.com/radiantearth/stac-spec/tree/v0.9.0/api-spec) features / extensions and [STAC extensions](https://github.com/radiantearth/stac-spec/tree/v0.9.0/extensions) can be implemented in addition to what is documented here. # noqa: E501

    :param limit: This parameter enables pagination for the endpoint and specifies the maximum number of elements that arrays in the top-level object (e.g. collections, processes, batch jobs, secondary services, log entries, etc.) are allowed to contain. The &#x60;links&#x60; array MUST NOT be paginated like the resources, but instead contain links related to the paginated resources or the pagination itself (e.g. a link to the next page). If the parameter is not provided or empty, all elements are returned.  Pagination is OPTIONAL: back-ends or clients may not support it. Therefore it MUST be implemented in a way that clients not supporting pagination get all resources regardless. Back-ends not supporting pagination MUST return all resources.  If the response is paginated, the &#x60;links&#x60; array MUST be used to communicate the links for browsing the pagination with pre-defined &#x60;rel&#x60; types. See the &#x60;links&#x60; array schema for supported &#x60;rel&#x60; types. Backend implementations can, unless specified otherwise, use all kind of pagination techniques, depending on what is supported best by their infrastructure: page-based, offset-based, token-based or something else. The clients SHOULD use whatever is specified in the links with the corresponding &#x60;rel&#x60; types.
    :type limit: int

    :rtype: Collections
    """
    collections = get_request(f"{earthsearch84}/collections", params={})
    return collections, requests.codes.ok


def describe_collection(collection_id):  # noqa: E501
    """Full metadata for a specific dataset from CLEOS Catalog

    Lists **all** information about a specific collection specified by the identifier &#x60;collection_id&#x60;.  This endpoint is compatible with [STAC 0.9.0](https://stacspec.org) and [OGC API - Features](http://docs.opengeospatial.org/is/17-069r3/17-069r3.html). [STAC API](https://github.com/radiantearth/stac-spec/tree/v0.9.0/api-spec) features / extensions and [STAC extensions](https://github.com/radiantearth/stac-spec/tree/v0.9.0/extensions) can be implemented in addition to what is documented here. # noqa: E501

    :param collection_id: Collection identifier
    :type collection_id: dict | bytes

    :rtype: InlineResponse200
    """
    try:
        collection = get_request(f"{earthsearch84}/collections/{collection_id}", params={})
        
        # Add eo:bands
        bands_ids = ["B01","B02","B03","B04","B05","B06","B07","B08","B8A","B09","B11","B12", "SCL", "AOT", "WVP"]
        bands = {"type": "bands", "values": bands_ids}
    
        eo_bands = collection.pop("item_assets")
        openeo_eo_bands = []
        item_assets = []

        for key, eo_band in eo_bands.items():
            if key in bands_ids:
                if "eo:bands" in eo_band:
                    openeo_eo_bands.append(eo_band["eo:bands"][0])
                else:
                    openeo_eo_bands.append({"name" : key})
            
                item_assets.append(eo_band)

        collection["summaries"]["eo:bands"] = openeo_eo_bands
        collection["item_assets"] = item_assets

        # Add datacube extension with dimensions
        collection["stac_extensions"].append("datacube")

        lon = [collection["extent"]["spatial"]["bbox"][0][0], collection["extent"]["spatial"]["bbox"][0][2]]
        lat = [collection["extent"]["spatial"]["bbox"][0][1], collection["extent"]["spatial"]["bbox"][0][3]]
        interval = collection["extent"]["temporal"]["interval"]

        x = {"type": "spatial","axis": "x","extent": lon, "reference_system": 4326}
        y = {"type": "spatial","axis": "y","extent": lat, "reference_system": 4326}
        t = {"type": "temporal", "extent": interval, "step": None}
        collection["cube:dimension"] = {"x": x, "y": y, "t": t, "bands": bands}

        return collection, requests.codes.ok
    except RuntimeError as e:
        if "Status '404'" in str(e):
            return {"message" : f"Collection with ID '{collection_id}' does not exist"}, requests.codes.not_found
        else:
            logger.exception()
            return {"message" : f"Unexpected error: {str(e)}"}, requests.codes.internal_server_error
