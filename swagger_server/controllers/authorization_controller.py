import flask
from typing import List
import base64
import requests
import json
import logging
from swagger_server.controllers.lib.auth import generate_jwt_token, decode_jwt_token

from connexion.exceptions import OAuthProblem

# TODO: Fix when an authentication db will be available 
with open('swagger_server/json/config.json') as f:
    user_config = json.load(f)

logger = logging.getLogger()


def authenticate_basic():  # noqa: E501
    """HTTP Basic authentication

    :rtype: HTTPBasicAccessToken
    """

    auth = flask.request.headers['Authorization']
    auth_type, auth_code = auth.split(" ")
    if auth_type != "Basic":
        return {"message" : f"Authorization type '{auth_type}' not supported"}, requests.codes.bad

    user_id, password = base64.b64decode(auth_code).decode("utf-8").split(":")

    return check_basic(user_id, password)


def check_basic(username, password, required_scopes=None):
    username = str(username)
    password = str(password) 

    if username in user_config and password == user_config[username]["password"]:
        # Generate new bearer token
        bearer_token = generate_jwt_token(username)
        return {"access_token": bearer_token}
    else:
        raise OAuthProblem('Invalid username or password')


def check_bearer(token):
    return {'uid' : decode_jwt_token(token)}