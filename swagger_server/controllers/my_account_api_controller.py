import json
import logging
import os
from string import Template
import glob

import connexion
from flask import send_file
import pystac
import requests
import shapely.geometry
from shapely.ops import cascaded_union
import six

from swagger_server.controllers.lib.auth import get_user_config_from_header

headers = {'Content-type': 'application/json'}

# logging configuration
logger = logging.getLogger()

with open('swagger_server/json/userdata.json') as f:
    response_userdata = Template(f.read())


def describe_account():  # noqa: E501
    """Information about the authenticated user

    This endpoint always returns the user id and MAY return the disk quota available to the user. It MAY also return links related to user management and the user profile, e.g. where payments are handled or the user profile could be edited. For back-ends that involve accounting, this service MAY also return the currently available money or credits in the currency the back-end is working with. This endpoint MAY be extended to fulfil the specification of the [OpenID Connect UserInfo Endpoint](http://openid.net/specs/openid-connect-core-1_0.html#UserInfo). # noqa: E501


    :rtype: UserData
    """
    user_id, _ = get_user_config_from_header()

    response_json = response_userdata.safe_substitute(user_id=user_id)
    return json.loads(response_json)


def list_jobs(limit=None):  # noqa: E501
    """List all batch jobs

    :rtype: BatchJob
    """

    user_id, _ = get_user_config_from_header()

    product_directory = f"swagger_server/jobs/{user_id}/"
    status_filenames = glob.glob(os.path.join(product_directory, "*", "status.json"))

    jobs_list = {"jobs" : [], "links" : []}

    # Check if user/product instance exists and load process graph
    if not status_filenames:
        return jobs_list, requests.codes.ok
    
    for status_filename in status_filenames:
        product_instance_id = os.path.dirname(status_filename)
        job_metadata = {"id" : product_instance_id}

        with open(status_filename, "r") as f:
            status_json = json.load(f)
            job_metadata["status"] = status_json["status"]
            job_metadata["created"] = status_json["created"]

        jobs_list["jobs"].append(job_metadata)

    return jobs_list



def describe_job(job_id):  # noqa: E501
    """Returns all information about a submitted batch job. # noqa: E501

    :rtype: BatchJob
    """

    user_id, _ = get_user_config_from_header()
    product_instance_id = job_id

    product_directory = f"swagger_server/jobs/{user_id}/{product_instance_id}"
    process_graph_filename = os.path.join(product_directory, "process_graph.json")
    status_filename = os.path.join(product_directory, "status.json")

    if os.path.exists(process_graph_filename):
        with open(process_graph_filename, "r") as f:
            process_graph = json.load(f)
    else:
        error_message = f"Error retrieving the job metadata. Job {product_instance_id} does not exist"
        logger.error(error_message)
        return {"message": error_message}, requests.codes.not_found

    job_metadata = {"id" : product_instance_id}
    job_metadata["process"] = process_graph["process"]

    if os.path.exists(status_filename):
        with open(status_filename, "r") as f:
            status = json.load(f)["status"]
            job_metadata["status"] = status
    else:
        job_metadata["status"] = "created"

    return job_metadata


def get_results(job_id):  # noqa: E501
    """Download results for a completed satellite imagery/processing request

    After finishing processing, this request will provide signed URLs to the processed files of the batch job with some additional metadata. The response is a [STAC Item (version 0.9.0)](https://github.com/radiantearth/stac-spec/tree/v0.9.0/item-spec) if it has spatial and temporal references included.  URL signing is a way to protect files from unauthorized access with a key in the URL instead of HTTP header based authorization. The URL signing key is similar to a password and its inclusion in the URL allows to download files using simple GET requests supported by a wide range of programs, e.g. web browsers or download managers. Back-ends are responsible to generate the URL signing keys and to manage their appropriate expiration. The back-end MAY indicate an expiration time by setting the &#x60;expires&#x60; property.  If processing has not finished yet requests to this endpoint MUST be rejected with openEO error &#x60;JobNotFinished&#x60;. # noqa: E501

    :param job_id: Unique job identifier.
    :type job_id: dict | bytes

    :rtype: BatchJobResult
    """

    user_id, _ = get_user_config_from_header()
    product_instance_id = job_id

    product_directory = f"swagger_server/jobs/{user_id}/{product_instance_id}"
    results_directory = os.path.join(product_directory, "results")
    
    status_filename = os.path.join(product_directory, "status.json")
    process_graph_filename = os.path.join(product_directory, "process_graph.json")

    output_filenames = glob.glob(os.path.join(results_directory, "*", "output.json"))

    # Check if user/product instance exists and load process graph
    if not os.path.exists(process_graph_filename):
        return {"message" : f"Job with ID '{job_id}' does not exist"}, requests.codes.not_found

    # Check if product instance is finished
    if os.path.exists(status_filename):
        with open(status_filename, "r") as f:
            status = json.load(f)["status"]
        
        if status == "queued":
            return {"id": product_instance_id, "level" : "error", "message" : f"Job with ID '{job_id}' is still in queue. Please try again later."}, requests.codes.bad
        
        elif status == "running":
            return {"id": product_instance_id, "level" : "info", "message" : f"Job with ID '{job_id}' has not finished computing the results yet. Please try again later"}, requests.codes.bad

        elif status == "error":
            return {"id": product_instance_id, "level" : "error", "message" : f"Job with ID '{job_id}' is failed"}, requests.codes.failed_dependency
    else:
        return {"id": product_instance_id, "level" : "info", "message" : f"Job with ID '{job_id}' has not been started yet."}, requests.codes.not_found
    
    # Extract output metadata
    dates = []
    geoms = []

    for output_filename in output_filenames:
        with open(output_filename, "r") as f:
            output = json.load(f)
        
        if output["type"] == "raster-datacube":
            dates.extend([t["datetime"] for t in output["dimensions"]["t"]])
            geoms.extend([shapely.geometry.asShape(g["geometry"]) for g in output["dimensions"]["geometry"]])

        elif output["type"] == "FeatureCollection":
            for feature in output["features"]:
                for datetime_key in ["datetime", "start_datetime", "end_datetime"]:
                    if datetime_key in feature["properties"] and feature["properties"][datetime_key] is not None:
                        dates.append(feature["properties"][datetime_key])

            geoms.extend([shapely.geometry.asShape(feature["geometry"]) for feature in output["features"]])
        
        else:
            return {"id": product_instance_id, "level" : "error", "message" : f"Backend is not able to extract job output metadata."}, requests.codes.internal_server_error

    geom = cascaded_union(geoms)
    bbox = list(geom.bounds)
    geom = shapely.geometry.mapping(geom.simplify(tolerance=0))

    item = pystac.Item(
        id = job_id,
        geometry = geom,
        bbox = bbox,
        datetime = None,
        properties = {
            "start_datetime" : min(dates),
            "end_datetime" : max(dates)
        })

    for result_filename in glob.glob(os.path.join(results_directory, "*", "*")):
        subpath = result_filename.split(results_directory + "/")[-1]
        href = f"/files/{product_instance_id}/results/{subpath}"
        item.add_asset(
            subpath,
            pystac.Asset(href = href, title = subpath)
        )
        
    return item.to_dict(), requests.codes.ok


def download_file(path):  # noqa: E501
    """Download a file from the workspace

    This service downloads a user files identified by its path relative to the user's root directory.
    If a folder is specified as path a `FileOperationUnsupported` error MUST be sent as response.
    
    :param path: Path of the file, relative to the user's root directory
    :type path: str | bytes
    """
    user_id, _ = get_user_config_from_header()
    filepath = f"swagger_server/jobs/{user_id}/{path}"

    if ".." in filepath:
         return {"message" : f"Path must not include relative references such as '.' and '..'"}, requests.codes.bad

    if not os.path.exists(filepath):
        return {"message" : f"Requested file does not exists."}, requests.codes.not_found

    try:
        filepath = filepath.split("swagger_server/")[-1]
        return send_file(filepath), requests.codes.ok
    except Exception as e:
        logger.exception(str(e))
        return {"message" : f"Requested file is temporarely unavailable."}, requests.codes.internal_server_error
