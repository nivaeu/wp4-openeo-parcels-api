import json
import logging
import os
from copy import deepcopy

import connexion
import requests

from openeo_pg_parser.translate import translate_process_graph

from swagger_server import logs
from swagger_server.processes.aggregate_spatial import aggregate_spatial
from swagger_server.processes.load_collection import load_collection
from swagger_server.processes.ndvi import ndvi
from swagger_server.processes.mask import mask
from swagger_server.processes.intrafield import intrafield
from swagger_server.processes.interfield import interfield
from swagger_server.processes.save_result import save_result

# TODO: move in a config file
LOCAL_AVAILABLE_PROCESSES = {
    "load_collection": load_collection,
    "ndvi": ndvi,
    "mask": mask,
    "aggregate_spatial": aggregate_spatial,
    "intrafield" : intrafield,
    "interfield" : interfield,
    "save_result": save_result
}

PROCESS_DEFS_DIRECTORY = f"swagger_server/processes/definitions/"

OPENEO_PORT = os.environ.get("OPENEO_PORT")
VALIDATION_ENDPOINT = f"http://localhost:{OPENEO_PORT}/api/v1.0/validation"

# logging configuration
logger = logging.getLogger()


def update_status(status_filename : str, status : str):
    """
    Update the job status
    """
    with open(status_filename, "r") as f:
        status_json = json.load(f)
    
    with open(status_filename, "w+") as f:
        status_json["status"] = status
        f.write(json.dumps(status_json))


def get_available_processes(user_config: dict, as_dict: bool=False):
    """
    Returns the available processes for a user given their auth token
    :param user_config: The dictionary of configurations for the user
    :param as_dict: if True returns the full metadata, if False returns only the list of the ids

    :return: integer|string
    """

    all_processes = list(LOCAL_AVAILABLE_PROCESSES.keys())
    available_processes_ids = []

    for process in all_processes:
        if process in user_config["processes"]:
            available_processes_ids.append(process)
    
    if as_dict:
        with open(f'swagger_server/json/processes.json', "r") as f:
            available_processes = json.load(f)

        for process in available_processes_ids:
            with open(f'swagger_server/processes/definitions/{process}.json', "r") as f:
                process_json = json.load(f)

            available_processes["processes"].append(process_json)

        return available_processes

    return available_processes_ids



def self_request_validate_job(body: dict, auth_header : str):
    """
    Check the job request calling internal "/validation" endpoint

    :param body: Job (process graph) to validate
    :param auth_header: Authorization header coming from user request
    """

    validation_response = requests.post(VALIDATION_ENDPOINT, json=body, headers={"Authorization": auth_header})
    results = validation_response.json()

    if validation_response.status_code == requests.codes.ok:
        if results["errors"]:
            error_message = json.dumps(results["errors"], indent=2)
            logger.error(json.dumps(results["errors"]))
            return {"code": "400", "message": error_message}, requests.codes.bad
    else:
        error_message = "Validation error: " + validation_response.text
        logger.error(error_message)
        return {"code": validation_response.status_code, "message": error_message}, validation_response.status_code


def execute_process_graph(body : dict, user_id : str, product_instance_id : str):
    """
    Execute the requested job

    :param body: Job (process graph) to execute
    """

    # adding context to log
    logs.log_new_product_instance_id(product_instance_id)

    try:
        product_directory = f"swagger_server/jobs/{user_id}/{product_instance_id}"
        results_directory = os.path.join(product_directory, "results")
        status_filename = os.path.join(product_directory, "status.json")

        os.makedirs(results_directory, exist_ok=True)

        # set status to running
        update_status(status_filename, "running")

        try:
            # Prepare output directory and graph
            process_graph = translate_process_graph(body, process_defs = PROCESS_DEFS_DIRECTORY).sort(by='dependency')

            # Run sequentially the processes
            results = []
            processing_outputs = {}

            for node in process_graph:
                args = deepcopy(node.arguments)

                for key, value in args.items():
                    if value and isinstance(value, dict) and "from_node" in value:
                        node_id = value["from_node"]
                        args[key] = processing_outputs[node_id]

                kwargs = {"arguments" : args}
                if node.process_id == "save_result":
                    output_directory = os.path.join(results_directory, node.name)
                    os.makedirs(output_directory, exist_ok=True)
                    kwargs["output_directory"] = output_directory
                    kwargs["is_result"] = node.is_result

                processing_outputs[node.id] = LOCAL_AVAILABLE_PROCESSES[node.process_id](**kwargs)

                if node.is_result:
                    results.append(processing_outputs[node.id])

            # set status to finished
            update_status(status_filename, "finished")

            return results, requests.codes.ok

        except Exception as e:
            logger.exception(str(e))
            # set status to error
            update_status(status_filename, "error")

    finally:
        #removing context from log
        logs.log_new_product_instance_id()
