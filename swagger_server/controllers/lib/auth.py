import datetime
import json
import logging

import connexion
import jwt
from connexion.exceptions import OAuthProblem
from copy import deepcopy

from swagger_server import SECRET_KEY

logger = logging.getLogger()

with open('swagger_server/json/config.json') as f:
    USER_CONFIG = json.load(f)

def generate_jwt_token(user_id):
    """
    Generates the Auth Token
    :param user_id:
    :return: string
    """
    payload = {
        'exp': datetime.datetime.utcnow() + datetime.timedelta(days=0, minutes=15),
        'iat': datetime.datetime.utcnow(),
        'sub': user_id
    }
    return jwt.encode(
        payload,
        SECRET_KEY,
        algorithm='HS256'
    )
    

def decode_jwt_token(auth_token):
    """
    Decodes the auth token
    :param auth_token:
    :return: integer|string
    """
    auth_token = auth_token.split("basic//")[-1]

    try:
        payload = jwt.decode(auth_token, SECRET_KEY, algorithms='HS256')
        return payload['sub']

    except jwt.ExpiredSignatureError:
        raise OAuthProblem('Signature expired. Please log in again')
    except jwt.InvalidTokenError:
        raise OAuthProblem('Invalid token. Please log in again.')


def get_user_config_from_header():
    """
    Returns the config dictionary of a user from their authorization header 
    :return: str,dict
    """

    auth_header = connexion.request.headers.get('Authorization')
    if auth_header:
        auth_token = auth_header.split(" ")[1]
        username = decode_jwt_token(auth_token)
    else:
        username = "public"
    
    user_config = deepcopy(USER_CONFIG[username])
    user_config.pop("password")

    return username, user_config
