import connexion
import six

from swagger_server import util

import requests
import json
import logging
from string import Template

# logging configuration
logger = logging.getLogger()

OPENEO_API_VERSION = '1.0.0'

with open('swagger_server/json/capabilities.json') as f:
    response_capabilities = Template(f.read())

with open('swagger_server/json/wellknown.json') as f:
    response_wellknown = Template(f.read())

with open('swagger_server/json/file_formats.json') as f:
    response_file_formats = json.load(f)



def capabilities():
    """Information about CLEOS

    Lists general information about CLEOS, including which version and endpoints of the openEO API are supported and billing information.

    :rtype: Capabilities
    """
    response_json = response_capabilities.safe_substitute(version=OPENEO_API_VERSION)
    return json.loads(response_json), requests.codes.ok


def connect():
    """Supported openEO versions

    Lists all implemented openEO versions supported by the service provider. This endpoint is the Well-Known URI (see [RFC 5785](https://tools.ietf.org/html/rfc5785)) for openEO.


    :rtype: WellKnownDiscovery
    """
    response_json = response_wellknown.safe_substitute(version=OPENEO_API_VERSION)
    return json.loads(response_json), requests.codes.ok


def list_file_types():
    """Supported file formats

    Lists supported input and output file formats.
    *Input* file formats specify which file a back-end can *read* from.
    *Output* file formats specify which file a back-end can *write* to.
    
    The response to this request is an object listing all available input and output file formats separately with their parameters and additional data. This endpoint does not include the supported secondary web services.
    
    **Note**: Format names and parameters are fully aligned with the GDAL codes if available, see [GDAL Raster Formats](http://www.gdal.org/formats_list.html) and [OGR Vector Formats](http://www.gdal.org/ogr_formats.html). It is OPTIONAL to support all output format parameters supported by GDAL. Some file formats not available through GDAL may be defined centrally for openEO.
    Custom file formats or parameters MAY be defined.  Format names are allowed to be *case insensitive* throughout the API.


    :rtype: FileFormats
    """
    return response_file_formats, requests.codes.ok

