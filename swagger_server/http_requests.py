import requests
import time
import random
import json
import logging
from typing import Optional

logger = logging.getLogger()

class TooManyRequestsException(Exception):
    """Exception to be raised when contacting an overloaded server"""
    pass

def get_request(
        url : str,
        params : dict,
        max_attempts : Optional[int] = 3,
        custom_timeout : Optional[int] = 30,
        backoff : Optional[int] = 2
    ):
    """
    GET requests to API

    :param url: The URL of API to send the request
    :param params: Request parameters to send to API
    :param max_attempts: Number of attempts to do before fail (excluded some exceptions)
    :param custom_timeout: Number of seconds to wait response from API and between requests
    :param backoff: Multiplication factor of timeout after each attempt
    """
    return _request("GET", url, params, {}, max_attempts, custom_timeout, backoff)


def delete_request(
        url : str,
        params : Optional[dict] = {},
        payload : Optional[dict] = {},
        max_attempts : Optional[int] = 3,
        custom_timeout : Optional[int] = 30,
        backoff : Optional[int] = 2
    ):
    """
    DELETE requests to API

    :param url: The URL of API to send the request
    :param params: Request parameters to send to API
    :param payload: Request payload to send to API
    :param max_attempts: Number of attempts to do before fail (excluded some exceptions)
    :param custom_timeout: Number of seconds to wait response from API and between requests
    :param backoff: Multiplication factor of timeout after each attempt
    """
    return _request("DELETE", url, params, payload, max_attempts, custom_timeout, backoff)


def post_request(
        url : str,
        params : Optional[dict] = {},
        payload : Optional[dict] = {},
        max_attempts : Optional[int] = 3,
        custom_timeout : Optional[int] = 30,
        backoff : Optional[int] = 2
    ):
    """
    POST requests to API

    :param url: The URL of API to send the request
    :param params: Request parameters to send to API
    :param payload: Request payload to send to API
    :param max_attempts: Number of attempts to do before fail (excluded some exceptions)
    :param custom_timeout: Number of seconds to wait response from API and between requests
    :param backoff: Multiplication factor of timeout after each attempt
    """
    return _request("POST", url, params, payload, max_attempts, custom_timeout, backoff)


def put_request(
        url : str,
        params : Optional[dict] = {},
        payload : Optional[dict] = {},
        max_attempts : Optional[int] = 3,
        custom_timeout : Optional[int] = 30,
        backoff : Optional[int] = 2
    ):
    """
    PUT requests to API

    :param url: The URL of API to send the request
    :param params: Request parameters to send to API
    :param payload: Request payload to send to API
    :param max_attempts: Number of attempts to do before fail (excluded some exceptions)
    :param custom_timeout: Number of seconds to wait response from API and between requests
    :param backoff: Multiplication factor of timeout after each attempt
    """
    return _request("PUT", url, params, payload, max_attempts, custom_timeout, backoff)


def _request(
        method : str,
        url : str,
        params : Optional[dict] = {},
        payload : Optional[dict] = {},
        max_attempts : Optional[int] = 3,
        custom_timeout : Optional[int] = 30,
        backoff : Optional[int] = 2
    ):
    """
    Function (private) to handle the several type of HTTP requests. 

    :param method: The HTTP method
    :param url: The URL of API to send the request
    :param params: Request parameters to send to API
    :param payload: Request payload to send to API
    :param max_attempts: Number of attempts to do before fail (excluded some exceptions)
    :param custom_timeout: Number of seconds to wait response from API and between requests
    :param backoff: Multiplication factor of timeout after each attempt
    """
        
    if method not in ["GET", "DELETE", "POST", "PUT"]:
        raise RuntimeError('Set method as "GET", "DELETE", "POST" or "PUT"')

    # logger.debug(f"{method} request to {url}")
    for attempt in range(max_attempts):
        try:
            # Little random sleep to avoid many requests at same time
            random_sleep = round(random.uniform(0.01, 1), 2)
            time.sleep(random_sleep)
            
            if method == "GET":
                r = requests.get(url, params = params, timeout=custom_timeout)
            elif method == "DELETE":
                r = requests.delete(url, params = params, timeout = custom_timeout)
            elif method == "POST":
                headers = {'Content-type': 'application/json'}
                r = requests.post(url, params = params,  json = payload, headers = headers, timeout = custom_timeout)
            elif method == "PUT":
                headers = {'Content-type': 'application/json'}
                r = requests.put(url, params = params, json = payload, headers = headers, timeout = custom_timeout)

            if r.status_code in [requests.codes.ok, requests.codes.created, requests.codes.accepted]:
                # logger.debug(f"{method} request successfull. Status '{r.status_code}' for POST to {url}")
                try: return json.loads(r.text)
                except json.JSONDecodeError: return r.text
            elif r.status_code == requests.codes.bad_request:
                raise RuntimeError(f"Status '400': Bad Request for {method} to {url}. message: {r.text}")
            elif r.status_code == requests.codes.not_found:
                raise RuntimeError(f"Status '404': {method} Not Found at {url}")
            elif r.status_code == requests.codes.too_many:
                raise exceptions.TooManyRequestsException(f"The server {url} is overloaded. Please retry later")
            else:
                logger.warning(f"Status '{r.status_code}' for {method} to {url}. message: {r.text}")
                
        except requests.exceptions.Timeout:
            logger.warning(f'TIMEOUT for {method} request to {url}. Waiting before retry.')

        except requests.exceptions.ConnectionError as e:
            logger.warning(str(e) + f' for {method} request to {url}. Waiting before retry.')

        if attempt < max_attempts-1:
            time.sleep(custom_timeout)
            custom_timeout = int(custom_timeout * backoff)

    raise RuntimeError(f"Max number of attempts reached: Request is wrong or Endpoint {url} is temporarly unavailable (check warnings)")
