import logging
import os
import sys

import connexion
from flask_cors import CORS
from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop
from tornado.wsgi import WSGIContainer

from swagger_server import encoder
from swagger_server.controllers.capabilities_api_controller import connect as openeo_well_known

logger = logging.getLogger()

class CorsHeaderMiddleware(object):
    def __init__(self, app):
        self.app = app

    def __call__(self, environ, start_response):
        def custom_start_response(status, headers, exc_info=None):
            # append whatever headers you need here
            headers.append(('Access-Control-Allow-Headers', 'Authorization,Content-Type'))
            headers.append(('Access-Control-Allow-Methods', 'OPTIONS,GET,POST,PATCH,PUT,DELETE'))
            headers.append(('Access-Control-Allow-Credentials', 'true'))
            headers.append(('Access-Control-Expose-Headers', 'Location,OpenEO-Identifier,OpenEO-Costs'))
            #headers.append(('Content-Type', 'application/json'))           
            return start_response(status, headers, exc_info)

        return self.app(environ, custom_start_response)


def main(port: int = 8080, swagger_file: str = 'niva.yaml'):
    """
    Main Launcher of the application
    :param port: Port on which the server should listen to type port: int
    """
    app = connexion.FlaskApp(__name__, specification_dir = 'swagger/')
    app.app.json_encoder = encoder.JSONEncoder
    app.add_api(swagger_file, strict_validation=True, arguments={'title': 'OpenEO'})
    app.app.wsgi_app = CorsHeaderMiddleware(app.app.wsgi_app)

    @app.route("/api/")
    def well_known_1():
        return openeo_well_known()
    
    @app.route("/api/.well-known/openeo")
    def well_known_2():
        return openeo_well_known()

    logger.info(f"Starting OpenEO API at PORT: {port}")
    http_server = HTTPServer(WSGIContainer(app))
    http_server.bind(port)
    cpu_count = os.cpu_count() // 4
    http_server.start(cpu_count if cpu_count > 1 else 2)
    IOLoop.current().start()


if __name__ == '__main__':
    try:
        port = int(sys.argv[1])
        swagger_file = str(sys.argv[2])

    except IndexError:
        logger.error("You need to provide the port, the swagger file path \
               and the launch mode ('local' or ) in order to run \
               the application: e.g. 'python3 -m swagger_server 8080 niva.yml")
        exit(1)
    os.environ["OPENEO_PORT"] = str(port)
    main(port, swagger_file)
